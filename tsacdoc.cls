\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tsacdoc}

\LoadClassWithOptions{article}
\ExecuteOptions{a4paper,10pt,twoside,onecolumn,final}
\ProcessOptions\relax

\RequirePackage[a4paper,top=60pt,bottom=60pt,left=60pt,right=60pt]{geometry}
\setlength{\parindent}{0pt}
\setlength{\parskip}{.5ex}

\RequirePackage[dutch]{babel}
\RequirePackage[T1]{fontenc}
\RequirePackage{erewhon}
\RequirePackage{tgheros}
\RequirePackage{microtype}

\RequirePackage{booktabs,tabularx,subfig,float}
\setlength{\heavyrulewidth}{1pt}

\RequirePackage[table,usenames,dvipsnames]{xcolor}
\colorlet{blue}{Blue}
\colorlet{red}{Red}
\colorlet{green}{Green}
\colorlet{brown}{Brown}
\colorlet{magenta}{Magenta}
\colorlet{purple}{Purple}
\colorlet{orange}{Orange}

\RequirePackage{tikz,pgfplots,pgf}
\pgfplotsset{compat=newest}
\usetikzlibrary{patterns,decorations.pathreplacing,shapes,arrows,positioning,hobby}
\tikzset{>=stealth}
\usetikzlibrary{
	scopes,
	calc,
	3d,
	arrows,
	calc,
	patterns,
	decorations.pathmorphing,
	decorations.markings,
	shapes
}

\RequirePackage{hyperref,cleveref}
\hypersetup{
	colorlinks=false,
	pdfborder={0 0 0},
}

\newcommand\email[1]{\href{mailto:#1}{#1}}

\def\nkbv{{NKBV}}
\def\nsac{{NSAC}}
\def\tsac{{TSAC}}
\def\kco{KaderCo}
\def\wco{Weekend-Commissaris}
\def\sc{Sportcentrum}
\def\zaalwacht{Zaalwacht}

\def\kvb{KVB}
\def\skb{SKB2}
\def\ski{SKI3}

\newcommand\printkvb[1]{\textcolor{blue}{\kvb{}-#1}}
\newcommand\printskb[1]{\textcolor{green}{\skb{}-#1}}
\newcommand\printski[1]{\textcolor{red}{\ski{}-#1}}
\newcommand\printopl[1]{\textcolor{orange}{#1}}
\newcommand\printold[1]{\textcolor{Brown}{#1}}

\def\kvbit{\printkvb{IT}}
\def\kvbiv{\printkvb{IV}}
\def\kvbov{\printkvb{OV}}
\def\kvbovmp{\printkvb{OV-MP}}
\def\kvbovmpt{\printkvb{OV-MP-Trad}}
\def\kvbovt{\printkvb{OV-Trad}}

\def\skbit{\printskb{IT}}
\def\skbo{\printskb{outdoor}}

\def\skiit{\printski{IT}}
\def\skiiv{\printski{IV}}
\def\skiov{\printski{OV}}
\def\skiovt{\printski{OV-Trad}}
\def\skiovmp{\printski{OV-MP}}
\def\skiovmpt{\printski{OV-MP-Trad}}

\def\pb{\printopl{PB}}
\def\lc{\printopl{LC}}
\def\be{\printopl{BE}}


\def\oldit{\printold{Toproper}}
\def\oldnk{\printold{Naklimmer}}
\def\oldgv{\printold{Gevorderde}}
\def\oldsc{\printold{ZK-SC}}
\def\oldov{\printold{ZK-OV}}
\def\oldak{\printold{ZK-AK}}
