Zaalreglement
=============

In this repository the "Zaalreglement" is managed.


## Contributing

If you see something that is unclear, or you want to change something feel free to file a new [issue](https://gitlab.com/tsac/zaalreglement/-/issues/new).

If you are an instructor and want to suggest a change to this document feel free to edit the LaTeX file and submit a new Merge Request. [Here is a way to do that from the browser.](https://gitlab.com/tsac/zaalreglement/-/edit/master/zaal_en_buitenwand_reglement.tex)

If you would like to add remarks to a suggested change you can find a list of suggested changes at [Merge Requests](https://gitlab.com/tsac/zaalreglement/-/merge_requests).

